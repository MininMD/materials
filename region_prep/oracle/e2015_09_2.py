#!/usr/bin/python
# -*- coding: UTF-8 -*-
from math import pi, sqrt
from random import uniform, normalvariate

l = uniform(0.25, 0.3)
g = 9.82
points = 20
step = round(l/2/points, 3)
k = 1.0
alpha = 1
beta = 1
A = 4*(pi*l)**2/g*(1./3+k/(1+k))/(1+k)
B = 4*pi**2/g

print "Расстояние, м\tПериод, с"
h = 0
while h < l/2:
    h += step
    x = normalvariate(h, 0.001)
    try:
        t = sqrt(A/x**alpha + B*x**beta)
    except ValueError:
        continue
    t = normalvariate(t, 0.01)
    if t < 0 or t > 3:
        continue
    print "%.3f\t%.2f" % (h, t)

