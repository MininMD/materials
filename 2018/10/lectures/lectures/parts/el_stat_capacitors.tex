\section{Ёмкость и конденсаторы}
\label{sec:capacity_and_capacitors}

Если зарядить проводник (например, сферу), его потенциал повысится (поскольку $\phi = k Q / r$). Для любого проводника $\phi
\sim Q$, поэтому можно ввести понятие \textbf{ёмкости}:

\begin{equation}
    \label{eq:def_capacity}
    C = \frac{q}{\phi}.
\end{equation}
Ёмкость измеряется в фарадах (в честь Майкла Фарадея). 

Например, ёмкость сферы равна $C = R / k = 4 \pi \eps_0 R$, пропорциональна радиусу. Можно оценить ёмкость Земли: $C = 9
\cdot 10^9 \cdot 6.4 \cdot 10^{6} \unit{м} \approx 700 \unit{мкФ}$. Видно, что даже такой большой объект как Земля имеет
довольно маленькую ёмкость.

Можно ли обобщить это понятие на случай нескольких проводников? Возьмём два незаряженных проводника произвольной формы и
соединим их батарейкой с напряжением $U$. Затем отключим батарейку.

\begin{figure}[ht]
    \centering
    \begin{tikzpicture}
        \pgfmathsetseed{3}
        \draw[fill = blue!20] plot [smooth cycle, samples = 8, domain = {1:8}] (\x * 360 / 8 + 5 * rnd:0.5cm + 1cm * rnd)
	        node[midway] {$+Q$};
        \pgfmathsetseed{4}
        \draw[xshift = 4cm, fill = red!20] plot [smooth cycle, samples = 8, domain = {9:16}]
        	(\x * 360 / 8 + 5 * rnd:0.5cm + 1cm*rnd) node[midway] {$-Q$};
    \end{tikzpicture}
    \caption{Два проводника, составляющие конденсатор}
    \label{fig:two_conductors_as_capacitor}
\end{figure}

В результате разность потенциалов между проводниками становится равна $U$. Предположим, что на одном проводнике при этом
скопился заряд $+Q$, а на другом~--- $-Q$. Можно ввести понятие \textbf{ёмкости конденсатора}:

\begin{equation}
    \label{eq:def_capacity_capacitor}
    C = \frac{Q}{U}. 
\end{equation}

Поскольку разность потенциалов пропорциональна заряду, то ёмкость является исключительно характеристикой конденсатора, его
геометрии. Проиллюстрируем это на примере плоского конденсатора.

\subsection{Пример: ёмкость плоского конденсатора.}
\label{sec:parallel_plate_capacitor}

\begin{wrapfigure}{l}{5cm}
    \vspace{-0.5cm}
    \begin{tikzpicture}
        \foreach \y in {0.5, 1.5, ..., 2.5}{
            \draw[very thick, blue, marrow] (0, \y) -- ++(2,0);
            \draw[very thick, blue, marrow] (2, \y) -- ++(1,0);
            \draw[very thick, blue, marrow] (0, \y) -- ++(-1,0);
        }

        \foreach \y in {0, 1, ..., 3}{
            \draw[very thick, red, marrow] (0, \y) -- ++(2, 0);
            \draw[very thick, red, marrow] (3, \y) -- ++(-1, 0);
            \draw[very thick, red, marrow] (-1, \y) -- ++(1, 0);
        }
        
        \draw[line width = 0.1cm] (0, -0.2) -- (0, 3.2) node[above] {$+Q$};
        \draw[line width = 0.1cm] (2, -0.2) -- (2, 3.2) node[above] {$-Q$};
    \end{tikzpicture}
\end{wrapfigure}


Рассмотрим две проводящие пластины площадью $S$, с зарядами $+Q$ и $-Q$. Пластины расположены на расстоянии $d$ друг от
друга. Если это расстояние много меньше, чем размеры пластин ($\sqrt{S} \gg d$), то мы можем считать их бесконечно большими.

Поэтому поле, создаваемое ими, можно считать однородным. Картина силовых линий этих полей показана на рисунке. Из него
вытекает, что слева и справа от пластин суммарное поле будет равно $0$~--- поля от положительно и отрицательно заряженных
пластин компенсируются. А вот между пластинами поле, наоборот, будет равно сумме полей:

\begin{equation}
    \label{ex:parallel_plate_capacitor_1}
    E = \frac{\sigma}{2 \eps_0} - \frac{-\sigma}{2 \eps_0} = \frac{\sigma}{\eps_0} = \frac{Q}{\eps_0 S}.
\end{equation}
Итак, между пластинами будет однородное поле с напряжённостью $E$. Это обеспечит разность потенциалов между пластинами, равную

\begin{equation}
    \label{ex:parallel_plate_capacitor_2}
    U = E \cdot d = \frac{Qd}{\eps_0 S}. 
\end{equation}
В соответствии с определением \eqref{eq:def_capacity_capacitor} получаем, что ёмкость плоского конденсатора равна

\begin{equation}
    \label{ex:parallel_plate_capacitor_3}
    C = \frac{\eps_0 S}{d}. 
\end{equation}

\subsection{Параллельное соединение конденсаторов}
\label{sec:capacitors_in_parallel}

\begin{wrapfigure}{l}{3cm}
    \vspace{-0.5cm}
    \begin{tikzpicture}[circuit ee IEC, thick, huge circuit symbols]
        \coordinate[contact] (A) at (0, 0);
        \coordinate[contact] (B) at (2, 0);
        \draw[thick] (A) -- ++(0,1.5) to[capacitor = {info' = {$C_1$}}] ++(2, 0) -- (B) to[capacitor = {info = {$C_2$}}]
        	++(-2, 0) -- (A);
        \draw[thick] (A) -- ++(0, -1.5) to[battery] ++(2, 0) -- (B);
  \end{tikzpicture}
\end{wrapfigure}

Посмотрим, что будет, если два конденсатора соединить параллельно, плюсом к плюсу. Вопрос ставится так: можно ли их заменить
одним конденсатором? Допустим, заряды на них $Q_1$ и $Q_2$, ёмкости $C_1$ и $C_2$ соответственно.

Если мы заменим два конденсатора одним (с зарядом $Q$ и ёмкостью $C$), то заряд на нём должен быть равен суммарному заряду $Q
= Q_1 + Q_2$. Напряжение при этом остаётся тем же самым, так что
\begin{equation}
    \label{eq:capacitors_in_parallel_1}
    U = \frac{Q_1}{C_1} = \frac{Q_2}{C_2} = \frac{Q}{C}. 
\end{equation}
Отсюда следует, что $C = C_1 + C_2$. Итак, при параллельном соединении конденсаторов их ёмкости складываются.

Очевидно, если параллельно соединить конденсаторов, то $C = C_1 + \ldots + C_{N}$. 

\subsection{Энергия, запасённая в конденсаторе}
\label{sec:capacitor_energy}

Рассмотрим конденсатор, состоящий из двух параллельных проводящих пластин площадью $S$, расстояние между которыми $d$ (как
обычно, $\sqrt{S} \gg d$). Вначале пластины не заряжены. Попробуем зарядить этот конденсатор следующим образом: возьмём
небольшой заряд $\Delta q$ с одной пластины и перенесём его на другую. Нам не придётся совершать никакой работы: ведь
поскольку пластины не заряжены, то поле между ними отсутствует.

Повторим эту процедуру. Теперь уже придётся совершить небольшую работу, так как между пластинами будет электрическое поле. По
мере увеличения заряда на пластинах переносить небольшие порции зарядабудет всё труднее. Подсчитаем работу, которая
необходима для зарядки пластин конденсатора до заряда $Q$.

Итак, пусть на обкладках конденсатора уже есть какой-то заряд $q$. Этот заряд создаёт напряжение $u = q / C$. Для того, чтобы
перенести заряд $\Delta q$ в таком напряжении, надо совершить работу

\begin{equation}
    \label{eq:capacitor_energy_1}
    \Delta A  = \Delta q u = \Delta q \frac{q}{C}.
\end{equation}
Значит, полная работа, необходимая для зарядки конденсатора равна

\begin{equation}
    \label{eq:capacitor_energy_2}
    A = \sum \Delta A = \frac{1}{C} \sum\limits_{q = 0}^{q = Q}  q \Delta q. 
\end{equation}
Встаёт вопрос о подсчёте этой суммы. Рассчитаем её графически.

\begin{figure}[ht]
    \centering
    \begin{tikzpicture}[
        /pgfplots/axis labels at tip/.style = {
            xlabel style = {
                at = {
                    (current axis.right of origin)
                },
                yshift = -2 ex,
                anchor = west,
                fill = white
            },
            ylabel style = {
                at = {
                    (current axis.above origin)
                },
                yshift=1.75ex,
                anchor = center}
        }]
        \begin{axis}[
            xmin = 0,
            xmax = 6,
            ymin = 0,
            ymax = 6,
            axis x line = bottom,
            axis y line = middle,
            minor tick num = 3,
            axis labels at tip,
            xlabel = {заряд},
            ylabel = {заряд},
            xticklabel = \empty,
            yticklabel = \empty,
            extra x ticks = {2.5, 5},
            extra y ticks = {2.5, 5},
            extra x tick labels = {$q$, $Q$},
            extra y tick labels = {$q$, $Q$},
            grid = both]
            \addplot[very thick, red, mark = none, samples = 2, domain = 0:5.5] {x};
            \draw[fill = red!20] (axis cs:2.5, 0) rectangle (axis cs:3, 2.5);
            \draw (axis cs:2.75, 0.25) node {{\small $\Delta q$}};
            \draw[thick, dashed] (axis cs:5, 0) |- (axis cs:0, 5);
            \draw[->] (axis cs:1.5, 3.5) node[above] {$\Delta A$} to[out = 270, in = 155] (axis cs:2.75, 1.5);
        \end{axis}
    \end{tikzpicture}
    \caption{Работа, необходимая для зарядки конденсатора}
    \label{fig:capacitor_energy}
\end{figure}

На этом графике площадь, закрашенная красным, равна $q \Delta q$~--- это площадь прямоугольника со сторонами $q$ и $\Delta
q$. Ясно, что при уменьшении $\Delta q$ эта площадь будет почти неотличима от площади трапеции под графиком (ошибка составит
$\Delta q^2 / 2$).

Если мы просуммируем все эти площади, как предписывает нам формула \eqref{eq:capacitor_energy_2}, то получится площадь под
наклонной линией, то есть, площадь равнобедренного прямоугольного треугольника с катетами $Q$. Таким образом,

\begin{equation}
    \label{eq:capacitor_energy_3}
    A = \frac{1}{C} \sum\limits_{q = 0}^{q = Q}  q \Delta q = \frac{1}{C} \frac{Q^2}{2} = \frac{Q^2}{2 C} =
	    \frac{C U^{2}}{2}.
\end{equation}

Итак, мы нашли работу, которую необходимо затратить, чтобы зарядить конденсатор ёмкости $C$ до заряда $Q$ (или до напряжения
$U$). Очевидно, что эта работа идёт на увеличение потенциальной энергии в конденсаторе. Таким образом, энергия запасённая в
конденсаторе, выражается формулой \eqref{eq:capacitor_energy_3}.

Ясно, что это работает для любого конденсатора, а не только плоского (в этом вычислении мы вообще не пользовались его
формой).

Посмотрим ещё на один вариант записи этой энергии. Вспомним, что $U = E d$, тогда получится, что энергия в конденсаторе

\begin{equation}
    \label{eq:capacitor_energy_4}
    W = \frac{1}{2} \eps_0 (S d) E^2 = \eps_0 V E^2,
\end{equation}
где $V$~--- объём конденсатора. Можно ввести понятие \textbf{плотности энергии}:

\begin{equation}
    \label{eq:capacitor_energy_5}
    w = \frac{W}{V} = \frac{1}{2} \eps_0 E^2. 
\end{equation}
Мы видим, что там, где есть электрическое поле, появляется и потенциальная энергия этого поля, пропорциональная $E^2$. Это
наблюдение верно не только для конденсатора, но и вообще для любой ситуации. 

\subsubsection{Пример: два конденсатора с перевёрнутой полярностью}
\label{sec:ex:two_capacitors}

Конденсаторы ёмкостями $C_1$ и $C_2$ ($C_1 > C_2$) заряжены до напряжения $U$. Затем их подключают друг к другу так, как
показано на схеме. Найти разность потенциалов $\phi_A - \phi_B$.

\begin{figure}[ht]
    \centering
    \begin{tikzpicture}[circuit ee IEC, thick, huge circuit symbols]
        \coordinate[contact, label = left:$A$] (A) at (0, 0);
        \coordinate[contact, label = right:$B$] (B) at (2, 0);
        \draw[thick] (A) -- ++(0, 1) to[capacitor = {info' = {$C_1$}, info = {$q_{1i}$}}] ++(2, 0) -- (B) -- ++(0, -1)
	        to[capacitor = {info = {$C_2$}, info' = {$q_{2i}$}}] ++(-2, 0) -- (A);
        \draw (0.7, 1.2) node {+} (1.3, 1.2) node {-};
        \draw (0.7, -1.2) node {-} (1.3, -1.2) node {+};
    \end{tikzpicture}
    \caption{Два конденсатора}
    \label{fig:two_capacitors_before}
\end{figure}

После того, как конденсаторы будут подключены друг к другу, заряды на пластинах перераспределяться пока потенциалы на
пластинах не сравняются. Из этого следует, что конденсатор $C_2$ перезарядится так, что на его левой пластине будет плюс, а
на правой минус. Обозначим заряды конденсаторов после перезарядки как $q_{1f}$ и $q_{2f}$. Запишем суммарный заряд до
перезарядки:

\begin{equation}
    \label{eq:ex:two_capacitors_1}
    q_i = q_{1i} + q_{2i} = C_1 U - C_2 U = (C_1 - C_2) U. 
\end{equation}

Конечный заряд:

\begin{equation}
    \label{eq:ex:two_capacitors_2}
    q_f = q_{1f} + q_{2f} = (C_1 + C_2) U_f. 
\end{equation}

Но система изолирована, а значит, суммарный заряд поменяться не может, $q_i = q_f$. Отсюда находим конечное напряжение на
конденсаторах (оно же~--- разность потенциалов $\phi_A - \phi_B$):

\begin{equation}
    \label{eq:ex:two_capacitors_3}
    U_f = \frac{C_1 - C_2}{C_1 + C_2}U. 
\end{equation}

Ответ на вопрос задачи мы нашли, но осталась небольшая тонкость. Подсчитаем энергию системы в начале
\begin{equation}
    \label{eq:ex:two_capacitors_4}
    W_i = \frac{1}{2} (C_1 + C_2) U^2
\end{equation}
и в конце:
\begin{equation}
    \label{eq:ex:two_capacitors_5}
    W_f = \frac{1}{2} (C_1 + C_2)U_f^2 = \frac{1}{2} \frac{(C_1 - C_2)^2}{C_1 + C_2} U.
\end{equation}
Видно, что
\begin{equation}
    \label{eq:ex:two_capacitors_6}
    \frac{W_f}{W_i} = \left( \frac{C_1 - C_2}{C_1 + C_2} \right)^2 < 1.
\end{equation}
Получается, что в ходе перезарядки часть энергии потерялась. Сейчас это не очень легко оценить, но выясняется, что это потери
на электромагнитное излучение. 

\clearpage